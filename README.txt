-Ready to use theme-Get a ready to use theme with no css/html/any other configurations
-HTML5 Integration
	1. New content-specific elements, like , , , ,
	2. Override Drupal's Form with calendar, date, time, email, url, search
-CSS3 Integration - Theme built with CSS3
-Responsive Integration - Web, Mobile, Tablets, anything else!!! Theme works in all the devices!
-HTML4 & HTML5 Tags settings in place - All the HTML tags will have settings where we can set the styling for each of the element on the website
-CSS2 & CSS3 features settings in place
	1.Settings for Border Radius, Box Shadow, Text Shadow, Border
	2.For each element of HTML we will have the setting to set the CSS related things
	3.For each section on the website(Header, Footer, Blocks, Panels,etc.) will have settings for CSS related things
-Handy Website Breadcrumb settings
	1.What seprator to use?
	2.Home page - I don't want to show the breadcrumb.
	3.I want to show the Page title in the breadcrumb.
	4.I want to show the Parent Menus in the breadcrumb.
	5.I want to show the taxonomy hierarchy in the breadcrumb.
-Regions - on the go
	1.Panels like structure to create the regions? Create regions anywhere(in header, footer or in other region.
	2.Regions have settings for Grids.
-Template Files - Just a click away
	1.Create any template with UI.
	2.Select (from the drop down?) which template(TPL) file you want to create (node.tpl.php, page.tpl.php, page-front.tpl.php, node-25.tpl.php)
	3.Create regions for particular template file and override the default regions.
-Block icons
	1.Get a setting in Block Add/Edit page to adjust the icons for a particular block.
	2.OR get a setting in the theme to upload an icon for each already enabled block.
